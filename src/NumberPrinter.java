/*
 * Berner Fachhochschule
 * Modul 8051
 * Uebung 02
 * Autor: Johannes Gnaegi
 * 
 * Programm welches die ersten 10 positiven Integer Anzeigt und anschliessend
 * ihre Summe auf der gleichen Zeile ausgibt. 
 * 
 */
public class NumberPrinter {

	/**
	 * Ausgabe der ersten 10 positiven Integer sowie
	 * Summe derer. 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("\nDie Summe der ganzen Zahlen von 1 bis 10 ist: ");
		System.out.print(1+2+3+4+5+6+7+8+9+10);
	}

}
